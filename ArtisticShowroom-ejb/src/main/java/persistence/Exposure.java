package persistence;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Exposure
 *
 */
@Entity

public class Exposure implements Serializable {

	
	private int id;
	private Date startDate;
	private Date endDate;
	private Survey survey;
	private List<Details> details;
	private static final long serialVersionUID = 1L;

	public Exposure() {
		super();
	}  
	
	@OneToMany
	public List<Details> getDetails() {
		return details;
	}


	public void setDetails(List<Details> details) {
		this.details = details;
	}


	@OneToOne
	public Survey getSurvey() {
		return survey;
	}



	public void setSurvey(Survey survey) {
		this.survey = survey;
	}



	@Id    
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
   
}
