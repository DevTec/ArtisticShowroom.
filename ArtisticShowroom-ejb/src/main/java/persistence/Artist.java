package persistence;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

import persistence.User;

/**
 * Entity implementation class for Entity: Artist
 *
 */
@Entity

public class Artist extends User implements Serializable {

	private int experience;
	private String description;
	private Subscription subscription;
	private List<Artwork> artworks;
	private List<Metting> mettings;
	private List<Specialty> specialtys;
	private static final long serialVersionUID = 1L;

	public Artist() {
		super();
	}

	@OneToMany(mappedBy = "artist")
	public List<Metting> getMettings() {
		return mettings;
	}

	public void setMettings(List<Metting> mettings) {
		this.mettings = mettings;
	}

	@ManyToMany
	public List<Specialty> getSpecialtys() {
		return specialtys;
	}

	public void setSpecialtys(List<Specialty> specialtys) {
		this.specialtys = specialtys;
	}

	@OneToMany
	public List<Artwork> getArtworks() {
		return artworks;
	}

	public void setArtworks(List<Artwork> artworks) {
		this.artworks = artworks;
	}

	@OneToOne
	public Subscription getSubscription() {
		return subscription;
	}

	public void setSubscription(Subscription subscription) {
		this.subscription = subscription;
	}

	public int getExperience() {
		return this.experience;
	}

	public void setExperience(int experience) {
		this.experience = experience;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
